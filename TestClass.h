//
// Created by Will Roethel on 11/21/15.
//

#ifndef CATCH_DEMO_CLION_TESTCLASS_H
#define CATCH_DEMO_CLION_TESTCLASS_H

#include <string>
using namespace std;

class TestClass {
public:
    string getTestString();

    int getTestSum(int, int);

};


#endif //CATCH_DEMO_CLION_TESTCLASS_H
