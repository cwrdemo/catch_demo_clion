//
// Created by Will Roethel on 11/21/15.
//

#include <stdio.h>
#include "catch.hpp"
#include "../TestClass.h"


TEST_CASE("Test getTestString", "[String]") {

    TestClass tc;
    REQUIRE(tc.getTestString() == "test");
}

TEST_CASE("Test getTestSum") {
    TestClass tc;
    REQUIRE(tc.getTestSum(1, 3) == 4);
}

